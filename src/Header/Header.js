import React from "react";
// import "./styles.css";
import {AppBar, Toolbar, IconButton, Typography, makeStyles} from "@material-ui/core";
import Menu from "@material-ui/icons/Menu";

export const useStyles = makeStyles(theme => ({
    header: {
        [theme.breakpoints.up('sm')]:{
            color: 'black',
            boxShadow: '0px 0px 0px 0px',
        },
        backgroundColor: 'transparent',  
    },
    
}));
 
export default function Header() {
    const classes = useStyles();
    return (
        <div className='App'>
            <AppBar position='sticky' className={classes.header}>
                <Toolbar>
                    <IconButton aria-label='app' color='inherit'>
                        <Menu />
                    </IconButton>
                    <Typography variant='h6'> Driver App </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}


