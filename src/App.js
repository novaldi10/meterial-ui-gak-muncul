// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Header from './Header/Header';
import Appnavbar from './Appnavbar'
import "./App.css";

function App() {
  return (
    <div>
      <Header />
      <Appnavbar />
    </div>
  );
}

export default App;
